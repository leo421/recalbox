## RECALBOX - SYSTEM SATELLAVIEW ##

Placez ici vos roms satellaview.

Les roms doivent avoir l'extension ".bs/.smc/.sfc/.zip"

Ce système supporte également les roms compressées au format .zip.
Attention toutefois, il ne s'agit que d'une archive. Les fichiers contenus dans les .zip doivent correspondre aux extensions citées précédemment.
De plus, chaque fichier .zip ne doit contenir qu'une seule rom compressée.